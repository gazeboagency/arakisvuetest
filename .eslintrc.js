module.exports = {
	root: true,
	env: {
		browser: true,
		node: true
	},
	parserOptions: {
		parser: "babel-eslint"
	},
	extends: [
		"prettier",
		"prettier/vue",
		"@nuxtjs",
		"plugin:nuxt/recommended",
		"plugin:vue/essential"
	],
	plugins: [
		"vue"
	],
	// add your custom rules here
	rules: {
		"semi": 0,
		"quotes": ["error", "double", {
			"avoidEscape": true
		}],
		"vue/max-attributes-per-line": "off",
		"vue/singleline-html-element-content-newline": "off",
		"skipBlankLines": "off",
		"no-mixed-spaces-and-tabs": ["error", "smart-tabs"],
		"no-tabs": ["error", {
			allowIndentationTabs: true
		}],
		"indent": ["error", "tab"],
		"space-before-function-paren": ["error", "never"]
	}
}
