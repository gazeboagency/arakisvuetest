export default function(req, res, next) {
	if (req.url !== "/") {
		next();
	} else {
		res.writeHead(301, {
			Location: "/blog"
		});
		res.end()
	}
}
