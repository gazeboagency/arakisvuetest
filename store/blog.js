export const state = () => ({
	// fetched logic added to prevent requesting articles every page change
	fetched: false,
	fetchFailed: false,
	articles: []
});

export const mutations = {
	fetchSuccessful(state, payload) {
		state.fetched = true;
		state.fetchFailed = false;
		state.articles = [];
		for (const article of payload) {
			state.articles.push(article);
		}
	},
	fetchFailed(state) {
		state.fetched = false;
		state.fetchFailed = true;
		state.articles = [];
	}
};

export const actions = {
	async fetchArticles({
		state,
		commit
	}) {
		if (state.fetched) {
			return;
		}

		try {
			const request = this.$axios.$get("https://newsapi.org/v2/top-headlines", {
				params: {
					country: "us",
					apiKey: "63ea4d3972b54d509ae8a2e31a4797e1"
				}
			});
			const response = await request;

			if (!response.status || response.status !== "ok") {
				throw new Error("Invalid api response!");
			}

			commit("fetchSuccessful", response.articles);
		} catch (error) {
			commit("fetchFailed");
		}
	}
};
